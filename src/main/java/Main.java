import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    private static String xmlPath = "src\\main\\resources\\pkg_method.xlsx";

    public static void main(String[] args) {
        // 读取Excel文件内容
        List<PkgMethodBean> readResult = ExcelReader.readExcel(xmlPath);
        if (readResult != null) {
            System.out.println("list size = " + readResult.size());
            if (!readResult.isEmpty()) {
                Set<PkgMethodBean> pkgSet = new HashSet<>();
                for (PkgMethodBean bean :readResult){
                    if(!pkgSet.contains(bean)){
                        String pkgName = bean.getPkgName().trim();
                        pkgSet.add(new PkgMethodBean(pkgName,""));
//                        System.out.println("pkgName = " + pkgName);
                    }
                }
                System.out.println(pkgSet.size());
                ProjectPkgReader.setPkgInExcelSet(pkgSet);
                List<String> pathList = new ArrayList<>();
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\android_maskable_layout-master\\android_maskable_layout-master\\library\\src\\main\\java\\com\\christophesmet\\android\\views\\maskableframelayout");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\android-gpuimage\\library\\src\\main\\java\\jp\\co\\cyberagent\\android\\gpuimage");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\Android-IMSI-Catcher-Detector\\AIMSICD\\src\\main\\java\\com\\secupwn\\aimsicd");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\android-multipicker-library\\multipicker\\src\\main\\java\\com\\kbeanie\\multipicker");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\Andromeda\\Andromeda-Lib\\src\\main\\java\\org\\qiyi\\video\\svg");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\BottomNavigationViewEx\\app\\src\\main\\java\\com\\ittianyu\\bottomnavigationviewexsample");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\CalendarView\\calendarview\\src\\main\\java\\com\\haibin\\calendarview");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\calendula\\Calendula\\src");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\CountdownView\\library\\src\\main\\java\\cn\\iwgang\\countdownview");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\DKVideoPlayer\\dkplayer-java\\src\\main\\java\\com\\dueeeke\\videoplayer");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\DKVideoPlayer\\dkplayer-players\\exo\\src\\main\\java\\com\\dueeeke\\videoplayer\\exo");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\DKVideoPlayer\\dkplayer-players\\ijk\\src\\main\\java\\com\\dueeeke\\videoplayer\\ijk");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\DKVideoPlayer\\dkplayer-ui\\src\\main\\java\\com\\dueeeke\\videocontroller");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\DKVideoPlayer\\dkplayer-videocache\\src\\main\\java\\com\\danikula\\videocache");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\easypermissions\\easypermissions\\src\\main\\java\\pub\\devrel\\easypermissions");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\Folivora\\folivora\\src\\main\\java\\cn\\cricin\\folivora");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\InkPageIndicator\\InkPageIndicator\\src\\main\\java\\com\\pixelcan\\inkpageindicator");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\JKeyboardPanelSwitch\\library\\src\\main\\java\\cn\\dreamtobe\\kpswitch");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-android\\src\\main\\java\\org\\koin\\android");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-android-ext\\src\\main\\java\\org\\koin\\android\\experimental\\dsl");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-android-scope\\src\\main\\java\\org\\koin\\android\\scope");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-android-viewmodel\\src\\main\\java\\org\\koin\\android\\viewmodel");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-core\\src\\main\\kotlin\\org\\koin");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\koin\\koin-projects\\koin-core-ext\\src\\main\\kotlin\\org\\koin\\experimental");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\kotterknife\\src\\main\\kotlin\\kotterknife");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\LeafPic\\app\\src");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\MarqueeViewLibrary\\marqueelibrary\\src\\main\\java\\com\\gongwen\\marqueen");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\material-about-library\\library\\src\\main\\java\\com\\danielstone\\materialaboutlibrary");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\NewbieGuide\\guide\\src\\main\\java\\com\\app\\hubert\\guide");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\smartTable\\form\\src\\main\\java\\com\\bin\\david\\form");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\transferee-master\\Transferee\\src\\main\\java\\com\\hitomi\\tilibrary");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\TrustKit-Android\\trustkit\\src\\main\\java\\com\\datatheorem\\android\\trustkit");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\VideoPlayerManager\\video-player-manager\\src\\main\\java\\com\\volokh\\danylo\\video_player_manager");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\ViewAnimator\\viewanimator\\src\\main\\java\\com\\github\\florent37\\viewanimator");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\wallpaperboard\\library\\src\\main\\java\\com\\dm\\wallpaper\\board");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\WaveInApp\\library\\src\\main\\java\\com\\cleveroad\\audiovisualization");
                pathList.add("C:\\Users\\Administrator\\IdeaProjects\\WaveInApp\\wallpaper\\src\\main\\java\\com\\cleveroad\\wallpaper");

                for(String str:pathList){
                    ProjectPkgReader pkgReader = new ProjectPkgReader("zz", str);
                    pkgReader.doCompared();
                }
            }
        }
    }


}
