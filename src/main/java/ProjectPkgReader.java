

import java.io.*;
import java.util.*;

public class ProjectPkgReader {

    private static Set<PkgMethodBean> pkgInExcelSet;

    public static Set<PkgMethodBean> getPkgInExcelSet() {
        return pkgInExcelSet;
    }

    public static void setPkgInExcelSet(Set<PkgMethodBean> pkgInExcelSet) {
        ProjectPkgReader.pkgInExcelSet = pkgInExcelSet;
    }

    private final Set<PkgMethodBean> projectPkgSet = new HashSet<>();
    private final Set<PkgMethodBean> pkgNoContainSet = new HashSet<>();
    private final String projectName;
    private final String projectPath;


    public ProjectPkgReader(String projectName, String projectPath) {
        this.projectName = projectName;
        this.projectPath = projectPath;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getProjectPath() {
        return projectPath;
    }

    private boolean comparing = false;

    public void doCompared() {
        if (!comparing) {
            comparing = true;
            scanClassFiles();
            if (!classFiles.isEmpty()) {
                for (File itemFile : classFiles) {
                    doPkgCompare(itemFile);
                }
            }
            comparing = false;
        }
    }

    private List<File> classFiles = new ArrayList<>();

    private void scanClassFiles() {
        File file = new File(projectPath);
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String fileName = pathname.getName();
//                System.out.println(fileName);
                boolean result = pathname.isFile() && (fileName.endsWith(".java") || fileName.endsWith(".kt"));
//                System.out.println(pathname.getName() + " " + result);
                return result;
            }
        });
        if (files != null && files.length > 0) {
            classFiles.addAll(Arrays.asList(files));
        }
    }

    private static final String endFlag = "{";
    private static final String importPrefix = "import android.";

    private void doPkgCompare(File file) {
        FileInputStream fileInputStream = null;
        BufferedReader bufferedReader = null;
        String str = null;
        try {
            fileInputStream = new FileInputStream(file);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            while ((str = bufferedReader.readLine()) != null) {
                //System.out.println("cur content: " + str);
                if (str.contains(endFlag)) {
                    break;
                } else if (str.startsWith(importPrefix)) {
                    String curPkg = str.substring(7).trim();
                    if (curPkg.endsWith(";")) {
                        curPkg = curPkg.substring(0, curPkg.length() - 1);
                    }
//                    System.out.println("cur android pkg: " + curPkg);
                    PkgMethodBean bean = new PkgMethodBean(curPkg, "");
                    if (!projectPkgSet.contains(bean)) {
                        if (pkgInExcelSet.contains(bean)) {
                            projectPkgSet.add(bean);
                        } else {
                           if(pkgNoContainSet.add(bean)){
                               System.out.println("find no contain: " + bean.getPkgName());
                           }
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("doPkgCompare failed:" + file.getName());
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException ignored) {

                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignored) {

                }
            }
        }
    }

}
